'use strict';

const config = require(`../../config`);
const Promise = require(`bluebird`);
const router = require(`express`).Router();
const chalk = require(`chalk`);
const body = require(`body-parser`);
const route = require(`../help/route`);
const control = require(`../control`);
let db = require(`../database`).then(connection => db = connection);

router.use(body.json());
router.use(body.urlencoded({extended: true}));

router.get(`/`, control.index);
router.get(`/err`, control.error);
router.get(`/fb`, (req, res) => {
  res.status(200).send(req.query[`hub.challenge`]);
});
router.post(`/fb`, route(function* (req, res) {
  yield db.collection(`req`).insert(req.body);
  res.status(200).send(`200 OK`);
}));

router.use((err, req, res, next) => {
  const errStack = err.stack.split(`\n`);
  const errLocation = errStack[1]
    .replace(`${config.root}/`, ``)
    .replace(/\\/g, `/`)
    .trim();

  config.log(`${chalk.red(err.message)} ${errLocation}`);
  return res.send(errStack[0]);
});

module.exports = router;
