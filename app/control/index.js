'use strict';

const route = require(`../help/route`);

module.exports = {
  index: route(function* (req, res) {
    res.send(`Hello there.`);
  }),

  error: route(function* () {
    throw new Error(`intended`);
  }),
};
