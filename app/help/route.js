'use strict';

const Promise = require(`bluebird`);

module.exports = genFn => (req, res, next) =>
  Promise.coroutine(genFn)(req, res, next)
    .catch(next);

// Original
// https://strongloop.com/strongblog/async-error-handling-expressjs-es7-promises-generators/
//module.exports = function wrap(genFn) {
//  var cr = Promise.coroutine(genFn)
//  return function (req, res, next) {
//    cr(req, res, next).catch(next);
//  };
//}
