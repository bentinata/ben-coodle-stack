'use strict';

const config = require(`../../config`);
const Promise = require(`bluebird`);
const MongoClient = require(`mongodb`).MongoClient;
const opt = {
  promiseLibrary: Promise,
};

module.exports = MongoClient.connect(config.db, opt);
