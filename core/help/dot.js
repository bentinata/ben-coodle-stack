'use strict';

const dot = require('dot');
const fs = require('fs');
const cache = {};

module.exports = function(path, option, callback) {
  const filename = path.replace(option.settings.views + '/', '');

  if (cache[filename]) {
    const compiled = cache[filename];
    return callback(null, compiled(option));
  }
  else {
    fs.readFile(path, 'utf-8', (err, str) => {
      if (err) return callback(err);

      const def = {
        load(filename) {
          const path = option.settings.views + '/' + filename + '.html';
          return fs.readFileSync(path, 'utf-8');
        },
      };

      const compiled = dot.compile(str, def);
      if (option.cache) cache[filename] = compiled;

      return callback(null, compiled(option));
    });
  }
}
