'use strict';

const { listen } = require(`./config`);
const { name, version } = require(`./package.json`);
const fs = require(`fs`);
const isString = require(`lodash/fp/isString`);
const app = require(`./app`);
const server = require(`http`).createServer(app);
const exit = require(`./exit`);

server.listen(listen, () => {
  const address = server.address();

  if (isString(address)) {
    console.log(`${name}-${version} ${address}`);
    exit(callback => fs.unlink(address, callback));
  } else {
    console.log(`${name}-${version} ${address.address}${address.port}`);
  }
})
