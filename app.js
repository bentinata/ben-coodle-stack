'use strict';

const config = require(`./config`);
const express = require(`express`);
const app = express();
let db = require(`./app/database`).then(con => db = con);

app.engine(`html`, require(`./app/help/view`));
app.set(`views`, `${__dirname}/app/view`);
app.set(`view engine`, `html`);

if (!config.production) {
  app.use(express.static(`${__dirname}/static`));
}

app.use(require(`./app/route`));

module.exports = app;
